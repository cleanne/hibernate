package Cadastro.Cadastro;


import Cadastro.Cliente;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    private static EntityManagerFactory entityManagerFactory;

    public static void main(String args[]){

    entityManagerFactory = Persistence.createEntityManagerFactory("hibernatejpa");

    Cliente cliente = new Cliente();
    cliente.setNome("Cleanne");
    cliente.setCPF("50035695400");
    cliente.setTelefone("985349853");
    cliente.setEmail("cleannecassiano@gmail.com");

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(cliente);
            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

            System.out.println("insert: " + e.getMessage());

        } finally {
            em.close();

        }
    }
}
